package org.bookdumps.poc.bookservice.controller;

import java.util.List;

import org.bookdumps.poc.bookservice.bean.Book;
import org.bookdumps.poc.bookservice.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping("/api/v1")
public class BookServiceController {

	@Autowired
	private BookService bookService;
	
	@GetMapping("/books")
	public List<Book> getBooks(){	
	return bookService.getAllBooks();	
	}
	
	@GetMapping("/books/{bookIdList}")
	public List<Book> getBooksForUser(@PathVariable String bookIdList){	
	return bookService.getBooksForUser(bookIdList);	
	}
}
