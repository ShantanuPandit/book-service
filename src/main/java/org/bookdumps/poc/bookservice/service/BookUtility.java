package org.bookdumps.poc.bookservice.service;

import java.util.ArrayList;
import java.util.List;

import org.bookdumps.poc.bookservice.bean.Book;

public class BookUtility {

	public static void filterBooksForUser(List<Book> bookList, String bookIdList) {
		List<Book> userBooks = new ArrayList<Book>();
		List<Integer> bookIds = getBookIds(bookIdList);

		if (null != bookIds && !bookIdList.isEmpty() && null != bookList && !bookList.isEmpty()) {
			for (Integer userBookID : bookIds) {
				for (Book book : bookList) {
					if (book.getBookId().equals(userBookID))
						userBooks.add(book);
				}
			}
		}
		bookList.clear();
		bookList.addAll(userBooks);

	}

	private static List<Integer> getBookIds(String bookIdList) {
		List<Integer> idList = new ArrayList<Integer>();
		for (String id : bookIdList.split("_")) {
			idList.add(Integer.parseInt(id));
		}
		return idList;
	}

}
