package org.bookdumps.poc.bookservice.service;

import java.util.List;

import org.bookdumps.poc.bookservice.bean.Book;
import org.bookdumps.poc.bookservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	
	public List<Book> getAllBooks(){	
		return bookRepository.findAll();	
			
		}

	public List<Book> getBooksForUser(String bookIdList) {
		List<Book> bookList= this.bookRepository.findAll();
		BookUtility.filterBooksForUser(bookList,bookIdList);

		return bookList;
	}
}
