package org.bookdumps.poc.bookservice.repository;

import org.bookdumps.poc.bookservice.bean.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

}
